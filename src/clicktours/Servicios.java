/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
secuencia|id_hotel|id_servicio|estado
 */
package clicktours;

/**
 *
 * @author Gloria
 */
public class Servicios {
    private int secuencia;
    private int id_hotel;
    private int id_servicio;
    private String estado;

    /** Constructor de la clase Servicios
     * @param secuencia variable de tipo int
     * @param id_hotel variable de tipo int
     * @param  id_servicio variable de tipo int
     * @param estado variable de tipo String
     */
    public Servicios(int secuencia, int id_hotel, int id_servicio, String estado) {
        this.secuencia = secuencia;
        this.id_hotel = id_hotel;
        this.id_servicio = id_servicio;
        this.estado = estado;
    }

    /** Método para obtener la secuencia
     * No recibe parámetros
     * @return secuencia variable de tipo int
     */
    public int getSecuencia() {
        return secuencia;
    }

    /** Método para cambiar la secuencia
     * @param secuencia variable de tipo int
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }

    /** Método para obtener el id del Hotel
     * No recibe parámetros
     * @return id_hotel variable de tipo int
     */
    public int getId_hotel() {
        return id_hotel;
    }

    /** Método para cambiar el id del Hotel
     * @param id_hotel variable de tipo int
     */
    public void setId_hotel(int id_hotel) {
        this.id_hotel = id_hotel;
    }

    /** Método para obtener el id del servicio
     * No recibe parámetros
     * @return id_servicio variable de tipo int
     */
    public int getId_servicio() {
        return id_servicio;
    }

    /** Método para cambiar el id del servicio
     * @param id_servicio variable de tipo int
     */
    public void setId_servicio(int id_servicio) {
        this.id_servicio = id_servicio;
    }

    /** Método para obtener el Estado
     * No recibe parámetros
     * @return estado variable de tipo String
     */
    public String getEstado() {
        return estado;
    }

    /** Método para cambiar el Estado
     * @param estado variable de tipo String
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
