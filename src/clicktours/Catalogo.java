/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clicktours;

/**
 *
 * @author Samantha
 */
public class Catalogo {
    private int idservicio;
    private String servicio;

    /** Constructor de la clase Catalogo
     * @param idservicio variable de tipo int
     * @param servicio variable de tipo String
     */
    public Catalogo(int idservicio, String servicio) {
        this.idservicio = idservicio;
        this.servicio = servicio;
    }

    /** Método para obtener el id del servicio
     * No recibe parámetros
     * @return idServicio variable de tipo int
     */
    public int getIdservicio() {
        return idservicio;
    }
    
   /** Método para obtener el servicio
     * No recibe parámetros
     * @return servicio variable de tipo String
     */
    public String getServicio() {
        return servicio;
    }

    /** Método para cambiar el id del servicio
     * @param idservicio variable de tipo int
     */
    public void setIdservicio(int idservicio) {
        this.idservicio = idservicio;
    }

    /** Método para cambiar el servicio
     * @param servicio variable de tipo String
     */
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }
    
    
    
}
